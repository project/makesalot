<?php

/**
 * @file
 * Provide Drush integration for more advanced make usage.
 */

/**
 * Implements hook_drush_command().
 */
function makesalot_drush_command() {
  $items['makesalot'] = array(
    'description' => 'Make all the things!',
    'aliases' => array(),
    'bootstrap' => DRUSH_BOOTSTRAP_NONE,
    'arguments' => array(
      'makefile' => 'Which makefile to use. Assumes project.make if none are specified.',
    ),
    'options' => array(
      'skipdump' => 'Do not create dump of MySQL database.',
      'buildname' => 'Name to use for this build, ignoring branch detection.',
    ),
  );
  return $items;
}

/**
 * Begins the build process.
 *
 * Wraps drush make, sql-dump.
 */
function drush_makesalot($makefile = "repository/project.make", $buildname = NULL) {
  // Use args if options aren't set correctly.
  // This mimics usage of 'drush make'.
  $buildname = drush_get_option('buildname', $buildname);
  $makefile  = drush_get_option('makefile', $makefile);

  // This assumes we are in the project root.
  $proj_root  = getcwd();
  echo "Currently working in " . $proj_root . "\n";

  $repository = $proj_root . "/repository";
  $builds     = $proj_root . "/builds";
  $shared     = $proj_root . "/shared";
  $sql_dumps  = $proj_root . "/sql-dumps";

  // ISO 8601 date (yyyy-mm-ddThh:mm:ss+00:00).
  $time = date("c");
  // Strip colons since some operating systems have problems with them.
  $time = str_replace(':', '', $time);
  

  // Make sure makefile exists.
  if (!file_exists($makefile)) {
    return drush_set_error('DRUSH_BAD_PATH', dt('Could not find makefile at !makefile.', array('!makefile' => $makefile)));
  }

  // Attempt to detect the git branch, or else use "nobranch".
  if (!is_null($buildname)) {
    $branch = $buildname;
  } else if (file_exists($repository . '/.git/HEAD')) {
    $branch = file($repository . '/.git/HEAD', FILE_IGNORE_NEW_LINES);
    $branch = end(explode('/', $branch[0]));
  }
  else {
    $branch = 'nobranch';
  }

  // Concatenate id from repository, timestamp, and buildname.
  $id = $branch . '--' . $time;

  // Dump database unless otherwise specified.
  if (!drush_get_option('skipdump') && file_exists($proj_root . '/www')) {
    // Bootstrap to current build.
    chdir($proj_root . '/www');
    
    if (drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_DATABASE)){
      // Dump database.
      drush_invoke_process('@self', 'sql-dump', array(), array('gzip' => 1, 'result-file' => $sql_dumps . '/sql-dump--' . $id . '.sql.gz'));
      drush_print(dt('SQL Export created at !location', array('!location', $sql_dumps . '/sql-dump--' . $id . '.sql.gz')));
    } else {
      // drush_bootstrap has already logged the error.
      return false;
    }
    chdir($proj_root);
  } else {
    drush_print(dt('Skipping SQL export.'));
  }

  // Make project.
  $new_build = $builds . '/build--' . $id;

  drush_invoke_process('@self', 'make', array($proj_root . '/' . $makefile, $new_build));

  // Install support files.
  if (file_exists($new_build)) {

    foreach (array('modules', 'themes', 'libraries') as $dir) {
      if (!makesalot_install_file($repository, $new_build, $dir, 'symlink', false)) {
        // Error already logged.
        return false;
      }
    }

    if (!makesalot_install_file($shared, $new_build, 'files', 'symlink', false)) {
      // Error already logged.
      return false;
    }

    foreach (array('private', 'private-files', 'private_files') as $private_dir) {
      if (!makesalot_install_file($shared, $new_build, $private_dir, 'symlink', false)) {
        // Error already logged.
        return false;
      }
    }

    if (!makesalot_install_file($shared, $new_build, 'tmp', 'symlink', false)) {
      // Error already logged.
      return false;
    }

    if (!makesalot_install_file($shared, $new_build, 'settings.local.php', 'copy', false)) {
      // Error already logged.
      return false;
    }

    if (!makesalot_install_file($repository, $new_build, 'settings.php', 'copy', false)) {
      // Error already logged.
      return false;
    }

    if (file_exists($proj_root . '/www')) {
      unlink($proj_root . '/www');
    }
    symlink($new_build, $proj_root . '/www');
  }
  else {
    return drush_set_error('DRUSH_BAD_PATH', dt('Build failed. Check your makefile.'));
  }

  return drush_print(dt('Makesalot has completed.') . "\n");
}

// Helper function to copy or symlink $file from $directory to $new_build.
function makesalot_install_file($directory, $new_build, $file, $command = 'copy', $required = true){
  if (file_exists("$directory/$file")) {
    $msg = dt('Installing !directory/!file', array(
      '!directory' => $directory,
      '!file' => $file,
    ));
    drush_print($msg . "\n");

    if ($command == 'symlink'){
      symlink("$directory/$file", "$new_build/sites/default/$file");
    } else {
      copy("$directory/$file", "$new_build/sites/default/$file");
    }
    if (!file_exists("$new_build/sites/default/$file")){
      drush_set_error(
        DRUSH_PATH_NO_WRITABLE,
        dt(
          'Could not write !file to !dir. Check the file permissions.',
          array(
            '!file' => "$directory/$file",
            '!dir' => "$new_build/sites/default"
          )
        )
      );
      return false;
    }
  } else if ($required) {
    drush_set_error(
      'DRUSH_BAD_PATH',
      dt(
        'Could not find !file at !directory.',
        array(
          '!file' => $file,
          '!directory' => $directory
        )
      )
    );
    return false;
  }
  return true;
}